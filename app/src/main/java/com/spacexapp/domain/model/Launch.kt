package com.spacexapp.domain.model

import java.util.Date

data class Launch(
    val rocketId: String,
    val missionName: String,
    val flightNumber: Int,
    val launchYear: Int,
    val launchDate: Date,
    val isSuccessful: Boolean?,
    val missionPatchImage: String?
)
