package com.spacexapp.domain.repository

import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import io.reactivex.Single

interface SpaceXMemoryDataSource {

    fun getRockets(): Single<List<Rocket>>

    fun cacheRockets(rockets: List<Rocket>)

    fun getLaunches(): Single<List<Launch>>

    fun cacheLaunches(launches: List<Launch>)

}
