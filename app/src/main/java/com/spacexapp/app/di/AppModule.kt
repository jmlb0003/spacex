package com.spacexapp.app.di

import android.content.Context
import com.spacexapp.app.SpaceXApplication
import com.spacexapp.app.utils.ErrorHandler
import com.spacexapp.app.utils.Schedulers
import com.spacexapp.app.utils.SharedPreferencesHandler
import com.spacexapp.app.utils.TasksSchedulers
import com.spacexapp.presentation.navigation.SpaceXNavigator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplicationContext(application: SpaceXApplication): Context = application

    @Provides
    @Singleton
    fun provideSharedPreferencesHandler(applicationContext: Context): SharedPreferencesHandler =
        SharedPreferencesHandler(applicationContext)

    @Provides
    @Singleton
    fun provideRxSchedulers(): TasksSchedulers = Schedulers()

    @Provides
    @Singleton
    fun provideErrorHandler(): ErrorHandler = ErrorHandler

    @Provides
    @Singleton
    fun provideNavigator(): SpaceXNavigator = SpaceXNavigator()

}
