package com.spacexapp.app.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.spacexapp.presentation.details.RocketDetailsViewModel
import com.spacexapp.presentation.list.RocketListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(RocketListViewModel::class)
    abstract fun bindRocketListViewModel(rocketListViewModel: RocketListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RocketDetailsViewModel::class)
    abstract fun bindRocketDetailsViewModel(rocketDetailsViewModel: RocketDetailsViewModel): ViewModel

}
