package com.spacexapp.app.di

import com.spacexapp.app.utils.ServiceGenerator
import com.spacexapp.data.network.RocketNetworkParser
import com.spacexapp.data.network.SpaceXApiClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideApiClient(): SpaceXApiClient =
        ServiceGenerator.createService(SpaceXApiClient::class.java)

    @Provides
    @Singleton
    fun provideRocketNetworkParser(): RocketNetworkParser = RocketNetworkParser

}
