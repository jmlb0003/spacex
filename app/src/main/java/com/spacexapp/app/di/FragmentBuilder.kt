package com.spacexapp.app.di

import com.spacexapp.presentation.details.RocketDetailsFragment
import com.spacexapp.presentation.list.RocketListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract fun bindRocketListFragment(): RocketListFragment

    @ContributesAndroidInjector
    abstract fun bindRocketDetailsFragment(): RocketDetailsFragment

}
