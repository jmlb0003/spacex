package com.spacexapp.app.di

import com.spacexapp.data.SpaceXRepositoryImpl
import com.spacexapp.data.local.SpaceXMemoryDataSourceImp
import com.spacexapp.data.network.SpaceXNetworkDataSourceImpl
import com.spacexapp.domain.repository.SpaceXMemoryDataSource
import com.spacexapp.domain.repository.SpaceXNetworkDataSource
import com.spacexapp.domain.repository.SpaceXRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideSpaceXRepository(repository: SpaceXRepositoryImpl): SpaceXRepository

    @Binds
    @Singleton
    abstract fun provideSpaceXNetworkDataSource(networkDataSource: SpaceXNetworkDataSourceImpl): SpaceXNetworkDataSource

    @Binds
    @Singleton
    abstract fun provideSpaceXMemoryDataSource(memoryDataSource: SpaceXMemoryDataSourceImp): SpaceXMemoryDataSource

}
