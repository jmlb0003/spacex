package com.spacexapp.app.di

import android.app.Application
import com.spacexapp.app.SpaceXApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        RepositoryModule::class,
        NetworkModule::class,
        ActivityBuilder::class]
)
interface AppComponent : AndroidInjector<SpaceXApplication> {

    fun inject(application: Application)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: SpaceXApplication): Builder

        fun build(): AppComponent
    }

}
