package com.spacexapp.app.utils

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Inflate a view from a ViewGroup, useful on Adapters
 */
internal fun ViewGroup.inflate(@LayoutRes layout: Int): View {
    val layoutInflater = LayoutInflater.from(context)
    return layoutInflater.inflate(layout, this, false)
}
