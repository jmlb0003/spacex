package com.spacexapp.app.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.spacexapp.R
import javax.inject.Inject

class SharedPreferencesHandler
@Inject constructor(context: Context) {

    private val sharedPreferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)
    private val welcomeDialogShownKey: String by lazy {
        context.getString(R.string.prefs_key_welcome_dialog_shown)
    }

    var isAlreadyShownWelcomeDialog: Boolean
        get() = getBoolean(welcomeDialogShownKey, false)
        set(isShownWelcomeDialog) {
            setBoolean(welcomeDialogShownKey, isShownWelcomeDialog)
        }

    private fun getBoolean(key: String, defaultValue: Boolean) =
        sharedPreferences.getBoolean(key, defaultValue)

    private fun setBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

}
