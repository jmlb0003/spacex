package com.spacexapp.app.utils

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

class Schedulers : TasksSchedulers {

    private val backgroundThreadInstance: Scheduler by lazy { io.reactivex.schedulers.Schedulers.io() }

    private val uiThreadInstance: Scheduler by lazy { AndroidSchedulers.mainThread() }

    override fun getBackgroundThread(): Scheduler = backgroundThreadInstance

    override fun getUiThread(): Scheduler = uiThreadInstance

}
