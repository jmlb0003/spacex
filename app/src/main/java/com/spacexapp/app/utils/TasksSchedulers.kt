package com.spacexapp.app.utils

import io.reactivex.Scheduler

interface TasksSchedulers {

    fun getBackgroundThread(): Scheduler

    fun getUiThread(): Scheduler

}
