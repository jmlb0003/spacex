package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class ThrustVacuum(
    @SerializedName("kN") val kN: Int,
    @SerializedName("lbf") val lbf: Int
)
