package com.spacexapp.data.network.model.launch

import com.google.gson.annotations.SerializedName

data class NetworkLaunch(
    @SerializedName("mission_name") val missionName: String,
    @SerializedName("launch_year") val launchYear: Int,
    @SerializedName("launch_date_unix") val launchDateUnix: Int,
    @SerializedName("launch_date_utc") val launchDateUtc: String,
    @SerializedName("launch_success") val launchSuccess: Boolean?,
    @SerializedName("flight_number") val flightNumber: Int,
    @SerializedName("mission_id") val missionId: List<String>,
    @SerializedName("links") val links: Links,
    @SerializedName("rocket") val rocket: Rocket
)
