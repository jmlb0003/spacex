package com.spacexapp.data.network.model.launch

import com.google.gson.annotations.SerializedName

data class Rocket(
    @SerializedName("rocket_id") val rocketId: String,
    @SerializedName("rocket_name") val rocketName: String
)
