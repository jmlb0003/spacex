package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class PayloadWeights(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("kg") val kg: Int,
    @SerializedName("lb") val lb: Int
)
