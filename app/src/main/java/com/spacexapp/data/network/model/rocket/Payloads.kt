package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class Payloads(
    @SerializedName("option_1") val option_1: String,
    @SerializedName("composite_fairing") val composite_fairing: CompositeFairing
)
