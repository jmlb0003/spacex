package com.spacexapp.data.network

import com.spacexapp.data.network.model.launch.NetworkLaunch
import com.spacexapp.data.network.model.rocket.NetworkRocket
import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import com.spacexapp.domain.repository.SpaceXNetworkDataSource
import io.reactivex.Single
import java.io.IOException
import javax.inject.Inject

class SpaceXNetworkDataSourceImpl
@Inject constructor(
    private val apiService: SpaceXApiClient,
    private val parser: RocketNetworkParser
) : SpaceXNetworkDataSource {

    override fun getRockets(): Single<List<Rocket>> {
        return Single.create<List<NetworkRocket>> { emitter ->
            try {
                apiService.getAllRockets().execute().body()?.let { response ->
                    emitter.onSuccess(response)
                }
            } catch (e: IOException) {
                emitter.onError(Throwable(e.message))
            }
            if (!emitter.isDisposed) {
                emitter.onError(Throwable("We had an issue downloading rockets"))
            }
        }.map { networkResponse ->
            parser.parseRocketsResponse(networkResponse)
        }
    }

    override fun getLaunches(): Single<List<Launch>> {
        return Single.create<List<NetworkLaunch>> { emitter ->
            try {
                apiService.getAllLaunches().execute().body()?.let { response ->
                    emitter.onSuccess(response)
                }
            } catch (e: IOException) {
                emitter.onError(Throwable(e.message))
            }
            if (!emitter.isDisposed) {
                emitter.onError(Throwable("We had an issue downloading launches"))
            }
        }.map { networkResponse ->
            parser.parseLaunchesResponse(networkResponse)
        }
    }

}
