package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class NetworkRocket(
    @SerializedName("id") val id: Int,
    @SerializedName("rocket_id") val rocketId: String,
    @SerializedName("rocket_name") val rocketName: String,
    @SerializedName("rocket_type") val rocketType: String,
    @SerializedName("engines") val engines: Engines,
    @SerializedName("active") val active: Boolean,
    @SerializedName("stages") val stages: Int,
    @SerializedName("boosters") val boosters: Int,
    @SerializedName("cost_per_launch") val costPerLaunch: Int,
    @SerializedName("success_rate_pct") val successRate: Int,
    @SerializedName("first_flight") val firstFlight: String,
    @SerializedName("country") val country: String,
    @SerializedName("company") val company: String,
    @SerializedName("height") val height: Height,
    @SerializedName("diameter") val diameter: Diameter,
    @SerializedName("mass") val mass: Mass,
    @SerializedName("payload_weights") val payloadWeights: List<PayloadWeights>,
    @SerializedName("first_stage") val firstStage: FirstStage,
    @SerializedName("second_stage") val secondStage: SecondStage,
    @SerializedName("landing_legs") val landingLegs: LandingLegs,
    @SerializedName("flickr_images") val flickrImages: List<String>,
    @SerializedName("wikipedia") val wikipedia: String,
    @SerializedName("description") val description: String
)
