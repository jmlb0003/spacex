package com.spacexapp.data.network.model.rocket

import com.google.gson.annotations.SerializedName

data class Engines(
    @SerializedName("number") val number: Int,
    @SerializedName("type") val type: String,
    @SerializedName("version") val version: String,
    @SerializedName("layout") val layout: String,
    @SerializedName("engine_loss_max") val engineLossMax: Int,
    @SerializedName("propellant_1") val propellant_1: String,
    @SerializedName("propellant_2") val propellant_2: String,
    @SerializedName("thrust_sea_level") val thrustSeaLevel: ThrustSeaLevel,
    @SerializedName("thrust_vacuum") val thrustVacuum: ThrustVacuum,
    @SerializedName("thrust_to_weight") val thrustToWeight: Double
)
