package com.spacexapp.data.network

import com.spacexapp.data.network.model.launch.NetworkLaunch
import com.spacexapp.data.network.model.rocket.NetworkRocket
import retrofit2.Call
import retrofit2.http.GET

interface SpaceXApiClient {

    @GET("rockets")
    fun getAllRockets(): Call<List<NetworkRocket>>

    @GET("launches")
    fun getAllLaunches(): Call<List<NetworkLaunch>>

}
