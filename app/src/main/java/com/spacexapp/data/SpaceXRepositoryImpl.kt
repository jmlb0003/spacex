package com.spacexapp.data

import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import com.spacexapp.domain.repository.SpaceXMemoryDataSource
import com.spacexapp.domain.repository.SpaceXNetworkDataSource
import com.spacexapp.domain.repository.SpaceXRepository
import io.reactivex.Single
import javax.inject.Inject

class SpaceXRepositoryImpl
@Inject constructor(
    private val networkDataSource: SpaceXNetworkDataSource,
    private val memoryDataSource: SpaceXMemoryDataSource
) : SpaceXRepository {

    override fun getRefreshedRockets() = networkDataSource.getRockets()

    override fun getRockets(): Single<List<Rocket>> =
        Single.concat(memoryDataSource.getRockets(), getRocketsFromNetworkDataSource())
            .filter { rockets -> rockets.isNotEmpty() }
            .first(emptyList())

    private fun getRocketsFromNetworkDataSource() =
        networkDataSource.getRockets()
            .doAfterSuccess { rockets ->
                memoryDataSource.cacheRockets(rockets)
            }

    override fun getLaunches(): Single<List<Launch>> =
        Single.concat(memoryDataSource.getLaunches(), getLaunchesFromNetworkDataSource())
            .filter { launches -> launches.isNotEmpty() }
            .first(emptyList())

    private fun getLaunchesFromNetworkDataSource() =
        networkDataSource.getLaunches()
            .doAfterSuccess { launches ->
                memoryDataSource.cacheLaunches(launches)
            }

}
