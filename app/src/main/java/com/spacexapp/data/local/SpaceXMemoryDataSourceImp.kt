package com.spacexapp.data.local

import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import com.spacexapp.domain.repository.SpaceXMemoryDataSource
import io.reactivex.Single
import javax.inject.Inject

class SpaceXMemoryDataSourceImp
@Inject constructor() : SpaceXMemoryDataSource {

    private val rocketsCache = mutableListOf<Rocket>()

    private val launchesCache = mutableListOf<Launch>()

    override fun getRockets(): Single<List<Rocket>> = Single.just(rocketsCache)

    override fun cacheRockets(rockets: List<Rocket>) {
        rocketsCache.clear()
        rocketsCache.addAll(rockets)
    }

    override fun getLaunches(): Single<List<Launch>> = Single.just(launchesCache)

    override fun cacheLaunches(launches: List<Launch>) {
        launchesCache.clear()
        launchesCache.addAll(launches)
    }

}
