package com.spacexapp.presentation.list.adapter

import android.support.v7.util.DiffUtil

object RocketDiffItemCallback : DiffUtil.ItemCallback<RocketListItem>() {

    override fun areItemsTheSame(oldItem: RocketListItem, newItem: RocketListItem) =
        when (oldItem) {
            is RocketListItem.Header -> newItem is RocketListItem.Header
            is RocketListItem.RocketItem -> newItem is RocketListItem.RocketItem
        }

    override fun areContentsTheSame(oldItem: RocketListItem, newItem: RocketListItem) =
        oldItem == newItem

}
