package com.spacexapp.presentation.list.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.spacexapp.R
import com.spacexapp.domain.model.Rocket
import kotlinx.android.synthetic.main.item_list_header.view.header_label
import kotlinx.android.synthetic.main.item_list_rocket.view.country_label
import kotlinx.android.synthetic.main.item_list_rocket.view.engines_count_label
import kotlinx.android.synthetic.main.item_list_rocket.view.rocket_name_label

sealed class RocketListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(rocketListItem: RocketListItem)

    class HeaderViewHolder(view: View) : RocketListViewHolder(view) {

        override fun bind(rocketListItem: RocketListItem) {
            val header = rocketListItem as? RocketListItem.Header
                ?: throw IllegalStateException("This view holder must be bind by a header item")

            with(itemView) {
                header_label.text = resources.getString(R.string.rocket_list_header)
            }
        }
    }

    class RocketItemViewHolder(
        view: View,
        private val rocketClickedListener: (Rocket) -> Unit
    ) : RocketListViewHolder(view) {

        override fun bind(rocketListItem: RocketListItem) {
            val rocket = (rocketListItem as? RocketListItem.RocketItem)?.rocket
                ?: throw IllegalStateException("This view holder must be bind by a rocket item")

            with(itemView) {
                rocket_name_label.text = rocket.name
                country_label.text = rocket.country
                val enginesCount = rocket.enginesCount
                engines_count_label.text = resources.getQuantityString(R.plurals.engines_counter, enginesCount, enginesCount)

                setOnClickListener {
                    rocketClickedListener(rocket)
                }
            }
        }
    }

}
