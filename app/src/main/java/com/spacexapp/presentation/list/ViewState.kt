package com.spacexapp.presentation.list

import com.spacexapp.presentation.list.adapter.RocketListItem

sealed class ViewState {

    object Welcome : ViewState()

    object Busy : ViewState()

    object Refreshing : ViewState()

    class Done(val rockets: List<RocketListItem>) : ViewState()

    class Error(val error: Throwable) : ViewState()

}
