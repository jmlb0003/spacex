package com.spacexapp.presentation.list.adapter

import com.spacexapp.domain.model.Rocket

sealed class RocketListItem {

    object Header : RocketListItem()

    class RocketItem(val rocket: Rocket) : RocketListItem()

}
