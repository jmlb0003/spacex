package com.spacexapp.presentation.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.spacexapp.R
import com.spacexapp.app.di.ViewModelFactory
import com.spacexapp.presentation.list.adapter.RocketListItem
import com.spacexapp.presentation.list.adapter.RocketsAdapter
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_rocket_list.rockets_recycler_view
import kotlinx.android.synthetic.main.fragment_rocket_list.swipe_to_refresh
import javax.inject.Inject

class RocketListFragment : Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(RocketListViewModel::class.java)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewState()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_rocket_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRocketList()
    }

    private fun setupRocketList() {
        with(rockets_recycler_view) {
            adapter = RocketsAdapter(viewModel::onRocketClicked)
        }
        swipe_to_refresh.setOnRefreshListener {
            viewModel.reLoadRockets()
        }
    }

    private fun observeViewState() {
        viewModel.getViewState().observe(viewLifecycleOwner, Observer<ViewState> { viewState ->
            when (viewState) {
                is ViewState.Refreshing -> showRefreshing()
                is ViewState.Done -> showListOfRockets(viewState.rockets)
            }
        })
    }

    private fun showRefreshing() {
        swipe_to_refresh.isRefreshing = true
    }

    private fun showListOfRockets(rockets: List<RocketListItem>) {
        (rockets_recycler_view.adapter as RocketsAdapter).submitList(rockets)

        swipe_to_refresh.isRefreshing = false
    }

}
