package com.spacexapp.presentation.list.adapter

import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.ViewGroup
import com.spacexapp.R
import com.spacexapp.app.utils.inflate
import com.spacexapp.domain.model.Rocket

private const val HEADER = 0
private const val ROCKET = 1

class RocketsAdapter(
    private val rocketClickedListener: (Rocket) -> Unit
) : ListAdapter<RocketListItem, RocketListViewHolder>(RocketDiffItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            HEADER -> RocketListViewHolder.HeaderViewHolder(parent.inflate(R.layout.item_list_header))
            else -> RocketListViewHolder.RocketItemViewHolder(
                parent.inflate(R.layout.item_list_rocket),
                rocketClickedListener
            )
        }

    override fun onBindViewHolder(holder: RocketListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int) =
        when (getItem(position)) {
            is RocketListItem.Header -> HEADER
            is RocketListItem.RocketItem -> ROCKET
        }

}
