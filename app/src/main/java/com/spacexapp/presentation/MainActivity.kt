package com.spacexapp.presentation

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.spacexapp.R
import com.spacexapp.app.di.ViewModelFactory
import com.spacexapp.domain.model.Rocket
import com.spacexapp.presentation.details.DetailsViewState
import com.spacexapp.presentation.details.RocketDetailsFragment
import com.spacexapp.presentation.details.RocketDetailsViewModel
import com.spacexapp.presentation.list.RocketListFragment
import com.spacexapp.presentation.list.RocketListViewModel
import com.spacexapp.presentation.list.ViewState
import com.spacexapp.presentation.navigation.reObserve
import com.spacexapp.presentation.welcome.WelcomeDialogFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.loading_view
import javax.inject.Inject

private const val ROCKET_LIST_SCREEN_TAG = "Fragment:RocketListFragment"
private const val ROCKET_DETAILS_SCREEN_TAG = "Fragment:RocketDetailsFragment"
private const val UP_NAVIGATION_ARROW_SHOWN: String = "Key:up_navigation_arrow_shown"

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector,
    WelcomeDialogFragment.WelcomeDialogListener {

    @Inject lateinit var viewModelFactory: ViewModelFactory

    @Inject lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    private var isUpNavigationArrowShown = false

    private val rocketListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(RocketListViewModel::class.java)
    }

    private val rocketDetailsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(RocketDetailsViewModel::class.java)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showRocketListFragment()

        rocketListViewModel.apply {
            observeListViewState(this)
            observeNavigatorTriggers()
        }
        rocketDetailsViewModel.apply {
            observeDetailsViewState(this)
        }
        if (savedInstanceState != null) {
            isUpNavigationArrowShown = savedInstanceState.getBoolean(UP_NAVIGATION_ARROW_SHOWN)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(isUpNavigationArrowShown)
    }

    override fun onPause() {
        super.onPause()
        supportFragmentManager.findFragmentByTag(WelcomeDialogFragment.TAG)?.let {
            (it as WelcomeDialogFragment).dismiss()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putBoolean(UP_NAVIGATION_ARROW_SHOWN, isUpNavigationArrowShown)
        super.onSaveInstanceState(outState)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
                // Do nothing
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        isUpNavigationArrowShown = false
        supportActionBar?.setDisplayHomeAsUpEnabled(isUpNavigationArrowShown)
        super.onBackPressed()
    }

    private fun observeListViewState(viewModel: RocketListViewModel) {
        viewModel.getViewState().observe(this, Observer<ViewState> { viewState ->
            when (viewState) {
                is ViewState.Busy -> displayLoading()
                is ViewState.Error -> showError(viewState.error)
                is ViewState.Welcome -> displayWelcomeDialog()
                is ViewState.Done -> hideLoading()
            }
        })
    }

    private fun observeDetailsViewState(viewModel: RocketDetailsViewModel) {
        viewModel.getViewState().observe(this, Observer<DetailsViewState> { viewState ->
            when (viewState) {
                is DetailsViewState.Busy -> displayLoading()
                is DetailsViewState.Error -> showError(viewState.error)
                is DetailsViewState.Done -> hideLoading()
            }
        })
    }

    private fun observeNavigatorTriggers() {
        rocketListViewModel.getNavigatorTriggerToDetails().reObserve(
            this,
            Observer { rocketToShow ->
                rocketToShow?.let { showDetailsFragment(rocketToShow) }
            })
    }

    private fun displayLoading() {
        loading_view.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        loading_view.visibility = View.GONE
    }

    private fun showError(error: Throwable) {
        // TODO develop a proper error state
        Toast.makeText(this, "There was an error: ${error.message}", Toast.LENGTH_SHORT).show()
        hideLoading()
    }

    private fun displayWelcomeDialog() {
        WelcomeDialogFragment().apply {
            show(supportFragmentManager, WelcomeDialogFragment.TAG)
        }
        hideLoading()
    }

    override fun onContinueClicked() {
        rocketListViewModel.apply {
            welcomeDialogShown()
        }
    }

    private fun showRocketListFragment() {
        val listFragment =
            supportFragmentManager.findFragmentByTag(ROCKET_LIST_SCREEN_TAG) as? RocketListFragment
        if (listFragment == null) {
            val newListFragment = RocketListFragment()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, newListFragment, ROCKET_LIST_SCREEN_TAG)
                .commit()
        }
    }

    private fun showDetailsFragment(rocket: Rocket) {
        val detailsFragment =
            supportFragmentManager.findFragmentByTag(ROCKET_DETAILS_SCREEN_TAG) as? RocketDetailsFragment
        if (detailsFragment == null) {
            val newDetailsFragment = RocketDetailsFragment.newInstance(rocket)
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, newDetailsFragment, ROCKET_DETAILS_SCREEN_TAG)
                .addToBackStack(ROCKET_DETAILS_SCREEN_TAG)
                .commit()
        }

        isUpNavigationArrowShown = true
        supportActionBar?.setDisplayHomeAsUpEnabled(isUpNavigationArrowShown)
    }

}
