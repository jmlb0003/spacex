package com.spacexapp.presentation.details.adapter

import android.support.v7.util.DiffUtil

object RocketDetailsDiffItemCallback : DiffUtil.ItemCallback<RocketDetailsListItem>() {

    override fun areItemsTheSame(oldItem: RocketDetailsListItem, newItem: RocketDetailsListItem) =
        when (oldItem) {
            is RocketDetailsListItem.LaunchesGraphic -> newItem is RocketDetailsListItem.LaunchesGraphic
            is RocketDetailsListItem.RocketDescription -> newItem is RocketDetailsListItem.RocketDescription
            is RocketDetailsListItem.RocketLaunches.YearOfLaunch ->
                areYearOfLaunchTheSame(oldItem, newItem)
            is RocketDetailsListItem.RocketLaunches.Launch -> areLaunchesTheSame(oldItem, newItem)
        }

    private fun areYearOfLaunchTheSame(
        oldItem: RocketDetailsListItem.RocketLaunches.YearOfLaunch,
        newItem: RocketDetailsListItem
    ) =
        newItem is RocketDetailsListItem.RocketLaunches.YearOfLaunch &&
            oldItem.year == newItem.year

    private fun areLaunchesTheSame(
        oldItem: RocketDetailsListItem.RocketLaunches.Launch,
        newItem: RocketDetailsListItem
    ) =
        newItem is RocketDetailsListItem.RocketLaunches.Launch &&
            oldItem.missionName == newItem.missionName &&
            oldItem.flightNumber == newItem.flightNumber

    override fun areContentsTheSame(
        oldItem: RocketDetailsListItem,
        newItem: RocketDetailsListItem
    ) =
        oldItem == newItem

}
