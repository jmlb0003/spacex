package com.spacexapp.presentation.details

import com.spacexapp.presentation.details.adapter.RocketDetailsListItem

sealed class DetailsViewState {

    object Busy : DetailsViewState()

    class Done(val rocketDetails: List<RocketDetailsListItem>) : DetailsViewState()

    class Error(val error: Throwable) : DetailsViewState()

}
