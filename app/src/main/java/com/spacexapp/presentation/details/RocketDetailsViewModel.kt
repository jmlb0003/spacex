package com.spacexapp.presentation.details

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.spacexapp.app.utils.ErrorHandler
import com.spacexapp.app.utils.TasksSchedulers
import com.spacexapp.domain.model.Launch
import com.spacexapp.domain.model.Rocket
import com.spacexapp.domain.repository.SpaceXRepository
import com.spacexapp.presentation.details.adapter.RocketDetailsListItem
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import java.util.TreeMap
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RocketDetailsViewModel
@Inject constructor(
    private val schedulers: TasksSchedulers,
    private val errorHandler: ErrorHandler,
    private val repository: SpaceXRepository
) : ViewModel() {

    private val viewState = MutableLiveData<DetailsViewState>()
    private val disposables = CompositeDisposable()

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun getViewState(): LiveData<DetailsViewState> = viewState

    fun loadRocketDetails(rocket: Rocket) {
        disposables += repository.getLaunches()
            .filterLaunchesPerRocket(rocket.id)
            .normalizeLaunchesData(rocket)
            .subscribeOn(schedulers.getBackgroundThread())
            .observeOn(schedulers.getUiThread())
            .doOnSubscribe { viewState.value = DetailsViewState.Busy }
            .subscribe(::handleRocketDetailsSuccess, ::handleRocketDetailsError)
    }

    private fun Single<List<Launch>>.filterLaunchesPerRocket(rocketId: String) =
        flattenAsFlowable { launches: List<Launch> -> launches }
            .filter { launch: Launch -> launch.rocketId == rocketId }
            .toList()

    private fun Single<List<Launch>>.normalizeLaunchesData(rocket: Rocket): Single<List<RocketDetailsListItem>> =
        map { t: List<Launch> ->

            val launches = prepareLaunchesToNormalization(t)
            listOf(
                getLaunchesPerYear(launches),
                RocketDetailsListItem.RocketDescription(rocket.description)
            ) +
                getLaunchListSections(launches)
        }

    private fun getLaunchesPerYear(launchesPerYear: Map<Int, List<Launch>>): RocketDetailsListItem.LaunchesGraphic {
        val launchesPerYearForGraphic = mutableListOf<Pair<Int, Int>>()

        launchesPerYear.forEach { launchesEntry ->
            val year = launchesEntry.key
            val count = launchesEntry.value.size

            launchesPerYearForGraphic.add(Pair(year, count))
        }


        return RocketDetailsListItem.LaunchesGraphic(launchesPerYearForGraphic)
    }

    private fun getLaunchListSections(launchesPerYear: Map<Int, List<Launch>>): List<RocketDetailsListItem> {
        val launchListItems = mutableListOf<RocketDetailsListItem.RocketLaunches>()

        launchesPerYear.forEach { launchesEntry: Map.Entry<Int, List<Launch>> ->
            launchListItems.add(RocketDetailsListItem.RocketLaunches.YearOfLaunch(launchesEntry.key))
            launchesEntry.value.forEach { launch ->
                launchListItems.add(
                    RocketDetailsListItem.RocketLaunches.Launch(
                        launch.missionName,
                        launch.flightNumber,
                        launch.launchDate,
                        launch.isSuccessful,
                        launch.missionPatchImage
                    )
                )
            }
        }

        return launchListItems
    }

    private fun prepareLaunchesToNormalization(launches: List<Launch>): Map<Int, List<Launch>> {
        val launchesPerYear = TreeMap<Int, MutableList<Launch>>()
        var year: Int

        launches.forEach { launch ->
            year = launch.launchYear

            if (launchesPerYear[year] == null) {
                launchesPerYear[year] = mutableListOf(launch)
            } else {
                launchesPerYear[year]?.add(launch)
            }
        }

        return launchesPerYear
    }

    private fun handleRocketDetailsSuccess(rocketDetailsSections: List<RocketDetailsListItem>) {
        viewState.postValue(DetailsViewState.Done(rocketDetailsSections))
    }

    private fun handleRocketDetailsError(error: Throwable) {
        errorHandler.handleNetworkResponseError(javaClass.simpleName, error)
        viewState.postValue(DetailsViewState.Error(error))
    }

}
